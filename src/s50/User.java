package s50;

public class User {
    private String password;
    private String username;
    private boolean enabled;
    //khởi tạo không tham số
    public User() {
        this.password = "12345";
        this.username = "default user";
        this.enabled = true;
    }
    //khởi tạo 1 tham số
    public User(String username) {
        this.password = "12345";
        this.username = username;
        this.enabled = true;
    }
    //khởi tạo đầy đủ tham số
    public User(String password, String username, boolean enabled) {
        this.password = password;
        this.username = username;
        this.enabled = enabled;
    }
    //getter and setter
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    @Override
    public String toString() {
        return "User [password=" + password + ", username=" + username + ", enabled=" + enabled + "]";
    }
    
}
